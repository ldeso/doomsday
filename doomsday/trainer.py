import random
import time
from doomsday.doomsday import doomsday
from doomsday.utils import leap, weekday


def trainer():
	YYYY = random.randint(1850 , 2150)

	MM = random.randint(1,12)

	if MM == 4 or MM == 6 or MM == 9 or MM == 11 :
		dmax = 30
	elif MM == 2 :
		if leap(YYYY) == True:
			dmax = 29
		else :
			dmax = 28
	else :
		dmax = 31

	DD = random.randint(28,dmax)

	MM = str(MM)
	if len(MM) == 1 :
		MM = "0" + MM

	DD = str(DD)
	if len(DD) == 1 :
		DD = "0" + DD

	date = (str(YYYY)+"-"+MM+"-"+DD)

	print ("La dato estas: " + date + " !")

	t = time.time()

	ans = input("Kiu estas la tago de tiu dato? \n")

	t = time.time() - t
	t = int(t) + int((t-int(t))*100)/100

	if weekday(int(ans)) == doomsday(date) :
		print ("Gratulon! \n t = " + str(t) + " s")
	else :
		print ("Ne… " + doomsday(date) + " estis la ĝusta respondo…" )
