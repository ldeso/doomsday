def leap(y):
	return (( y % 4 == 0 and y % 100 != 0 ) or y % 400 == 0)

def weekday(A):
#Weekday
	if A % 7 == 0 :
		return ("Dimanĉo");
	if A % 7 == 1 :
		return ("Lundo")
	if A % 7 == 2 :
		return ("Mardo")
	if A % 7 == 3 :
		return ("Merkredo")
	if A % 7 == 4 :
		return ("Ĵaŭdo")
	if A % 7 == 5 :
		return ("Vendredo")
	if A % 7 == 6 :
		return ("Sabato")

def anchor(y):
#Century
	c = int(y/100)
#Century’s Anchor day
	if c % 4 == 0 :
		a = 2
	if c % 4 == 1 :
		a = 0
	if c % 4 == 2 :
		a = 5
	if c % 4 == 3 :
		a = 3
#2digits’ Anchor day
	i = y - c*100
	if i % 2 == 1 :
		i = i+11
	i = i / 2
	if i % 2 == 1 :
		i = i+11
	i = -i % 7
#Year’s Anchor weekday
	return(int((a+i)%7))

def key(y,m):
	if m > 2 :
		if m%2 == 0 :
			d = m
		elif m == 11 :
			d = 7
		elif m == 9 :
			d = 5
		elif m == 7 :
			d = 11
		elif m == 5 :
			d = 9
		elif m == 3 :
			d = 0
	if m == 2 :
		if leap(y) == True :
			d = 1
		else :
			d = 0
	if m == 1 :
		if leap(y) == True :
			d = 4
		else :
			d = 3
#Month’s key
	return (d)
